@extends('layout.master')
@section('title','Cast Index Page')
@section('content')
 <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Cast</h1>
                </div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Cast</li>
                    </ol>
                </div><!-- /.col -->
            </div>
        </div>
    </div>
 <!-- End Content Header (Page header) -->
 <!-- Main content -->
 <section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        List Of Cast
                    </div>
                    <div class="card-body">
                        @if (session('success'))
                            <div class="alert alert-success" role="alert">
                                {{session('success')}}
                            </div>
                        @endif
                        <a href={{Route('Cast.create')}} class="btn btn-info mb-3"><i class="fa fa-plus"> Create</i></a>
                        <table id="cast-table" class="table table-bordered table table-striped ">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>NAMA</th>
                                    <th>UMUR</th>
                                    <th>BIO</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach ($casts as $cast)
                                   <tr>
                                       <td>{{ $loop->iteration }}</td>
                                       <td> {{ $cast->nama}}</td>
                                       <td> {{ $cast->umur}}</td>
                                       <td> {{ $cast->bio}}</td>
                                       <td style="display:flex">
                                            <a href=" {{route('Cast.show',['Cast' => $cast->id])}} " class="btn btn-info mr-2"> show</a>
                                            <a href=" {{route('Cast.show',['Cast' => $cast->id])}}/edit" class="btn btn-success mr-2"> Edit</a>
                                            <form action="{{route('Cast.destroy',['Cast' => $cast->id])}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" value="Delete" class="btn btn-danger"/>
                                            </form>
                                        </th>
                                   </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
 </section>
@endsection
