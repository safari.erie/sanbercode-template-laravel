@extends('layout.master')
@section('title','Cast Create Page')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">  Form Cast </h1>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary">
                        <div class="card-header">
                            Create
                        </div>
                        <div class="card-body">
                            <form role="form" action="{{ route('Cast.store')}}" method="post">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama','')}}"
                                        placeholder="Enter Nama">
                                        @error('nama')
                                            <div class="alert alert-danger"> {{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="umur">Umur</label>
                                        <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur','')}}"
                                        placeholder="Enter Umur">
                                         @error('umur')
                                            <div class="alert alert-danger"> {{$message}}</div>
                                        @enderror
                                    </div>
                                     <div class="form-group">
                                        <label for="bio">Bio</label>
                                        <textarea name="bio" id="bio" name="bio" cols="30" rows="10" class="form-control" value="{{ old('bio','')}}"></textarea>
                                         @error('nama')
                                            <div class="alert alert-danger"> {{$message}}</div>
                                        @enderror
                                    </div>

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href={{Route('Cast.index')}} class="btn btn-dark">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
