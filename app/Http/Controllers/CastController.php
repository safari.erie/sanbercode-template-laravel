<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;
class CastController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->except(['index']);
    }
    /**
     * method index
     */
    public function index(){
        // use Query builder
        //$dataCasts = DB::table('casts')->get();
        // use ORM Elequent (model)
        $dataCasts = Cast::all();
        //dd($dataCasts);
        return view('cast.index',['casts'=>$dataCasts]);
    }
    /**
     * method get create
     * description : show form create post
     */

     public function create(){

        return view('cast.create');
     }

     /*
     * method post
     * description for insert to database
     *   */
    public function store(Request $request){

        $validated = $request->validate([
            'nama' => 'required|unique:casts|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);
        /* $postForm = [
            'nama'  => $request['nama'],
            'umur'  => $request['umur'],
            'bio'  => $request['bio'],
            'created_at' => date('Y-m-d H:i:s')
        ]; */

        // create object entity casts
        $cast = new Cast;
        $cast->nama = $request['nama'];
        $cast->umur = $request['umur'];
        $cast->bio = $request['bio'];
        $cast->created_at = date('Y-m-d H:i:s');
        $cast->save();
        //$insert = DB::table('casts')->insert($postForm);

        return redirect('/Cast')->with('success','Data Cast Berhasil Disimpan!');
    }

    public function show($id){
        // use query builder
        //$castById = DB::table('casts')->where('id',$id)->first();
        //dd($castById);
        // use elequent
        $castById = Cast::find($id);
        return view('cast.show',compact('castById'));
    }

    public function edit($id){
        // use query builder
        //$castById = DB::table('casts')->where('id',$id)->first();
        //dd($castById);
        // use elequent
        $castById = Cast::find($id);
        return view('cast.edit',compact('castById'));
    }

    public function update($id, Request $request){
        $validated = $request->validate([
            'umur' => 'required',
            'bio' => 'required',
        ]);
        // use query builder
       /*  $postFormUpdate = [
            'nama'  => $request['nama'],
            'umur'  => $request['umur'],
            'bio'  => $request['bio'],
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $update = DB::table('casts')
                    ->where('id',$id)
                    ->update($postFormUpdate); */

        //dd($castById);
        // use elequent model
        $postFormUpdate = [
            'nama'  => $request['nama'],
            'umur'  => $request['umur'],
            'bio'  => $request['bio'],
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $update = Cast::where('id', $id)->update(
            $postFormUpdate
        );
        return redirect('/Cast')->with('success','Data Cast Berhasil Dirubah!');

    }

    public function destroy($id){
        // use query builder
        /*  $delete = DB::table('casts')->where('id',$id)->delete(); */
        Cast::destroy($id);
         return redirect('/Cast')->with('success','Data Cast Berhasil Dihapus!');

    }

}
