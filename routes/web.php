<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboards.index');
    //return view('welcome');
});


Route::get('/table',function(){
    return view('tables.table');
});

Route::get('/data-table',function(){
    return view('tables.datatables');
});

/**
 * route all for cast
 */
/* Route::get('/cast','castcontroller@index');
Route::get('/cast/create','castcontroller@create');
Route::post('/store','castcontroller@store');
Route::get('/casts/{id}','castcontroller@show');
Route::get('/casts/{id}/edit','castcontroller@edit');
Route::put('/store/{id}','castcontroller@update');
Route::delete('/cast/{id}','castcontroller@destroy'); */
Route::resource('Cast','CastController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
